package edu.upenn.cis455.mapreduce.job;

import java.util.Iterator;

import edu.upenn.cis455.mapreduce.Context;
import edu.upenn.cis455.mapreduce.Job;
import edu.upenn.cis455.mapreduce.worker.WorkerServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WordCount implements Job {
	static Logger log = LogManager.getLogger(WordCount.class);

	/**
	 * This is a method that lets us call map while recording the StormLite source executor ID.
	 * 
	 */
	public void map(String key, String value, Context context, String sourceExecutor)
	{
		log.info("Mapper activated {" + key + ", " + value + "}");
		context.write(value, value, sourceExecutor);
	}

	/**
	 * This is a method that lets us call map while recording the StormLite source executor ID.
	 * 
	 */
	public void reduce(String key, Iterator<String> values, Context context, String sourceExecutor)
	{
		log.info("Reducer activated");
		int count = 0;

		for ( ; values.hasNext() ; ++count ) values.next();
		context.write(key, String.valueOf(count), sourceExecutor);
		// Your reduce function goes here
	}

}
