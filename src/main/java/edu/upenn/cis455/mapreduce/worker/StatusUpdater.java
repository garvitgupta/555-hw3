package edu.upenn.cis455.mapreduce.worker;

import edu.upenn.cis.stormlite.Config;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class StatusUpdater implements Runnable {

    private final String masterAddress;

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */

    StatusUpdater(String masterAddress) {
        this.masterAddress = masterAddress;
    }

    @Override
    public void run() {

        while(true) {
            try {
                sendGET("hey");
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Function to invoke push data on a given worker [Manual]
     */
    private HttpURLConnection sendGET(String parameters) throws IOException {

        System.out.println("Sending GET request to " + this.masterAddress + "/workerstatus");
        URL url = new URL(this.masterAddress + "/workerstatus");

        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("GET");
        conn.getOutputStream();

        return conn;
    }
}
