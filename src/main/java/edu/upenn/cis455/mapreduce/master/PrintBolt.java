package edu.upenn.cis455.mapreduce.master;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.distributed.ConsensusTracker;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;

/**
 * A trivial bolt that simply outputs its input stream to the
 * console
 *
 * @author zives
 *
 */
public class PrintBolt implements IRichBolt {
    static Logger log = LogManager.getLogger(PrintBolt.class);

    Fields myFields = new Fields();

    /**
     * To make it easier to debug: we have a unique ID for each
     * instance of the PrintBolt, aka each "executor"
     */
    String executorId = UUID.randomUUID().toString();
    private Map<String, String> config;

    @Override
    public void cleanup() {
        // Do nothing

    }

    @Override
    public boolean execute(Tuple input) {
        if (!input.isEndOfStream()) {
            log.info(getExecutorId() + ": " + input.toString());

            try {
                String outputFile = config.get("output") + "/output.txt";
                writeOutput(input, outputFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    @Override
    public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
        this.config = stormConf;
    }

    @Override
    public String getExecutorId() {
        return executorId;
    }

    @Override
    public void setRouter(StreamRouter router) {
        // Do nothing
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(myFields);
    }

    @Override
    public Fields getSchema() {
        return myFields;
    }

    /**
     * Writes the input tuple to an output.txt in the output dir
     * @param input
     * @param output
     */
    private void writeOutput(Tuple input, String output) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(output, true));

        String key = input.getStringByField("key");
        String value = input.getStringByField("value");

        bufferedWriter.write(key + ", " + value);
        bufferedWriter.newLine();
        bufferedWriter.close();
    }

}
