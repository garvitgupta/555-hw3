package edu.upenn.cis455.mapreduce.master;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.WorkerStatus;
import edu.upenn.cis.stormlite.bolt.MapBolt;
import edu.upenn.cis.stormlite.bolt.ReduceBolt;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.spout.FileSpout;
import edu.upenn.cis.stormlite.tuple.Fields;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

import static edu.upenn.cis.stormlite.Utilities.*;
import static spark.Spark.*;

public class MasterServer {

    static Logger log = LogManager.getLogger(MasterServer.class);

    static final long serialVersionUID = 455555001;
    private static final int myPort = 8000;
    private enum Status { idle, mapping, reducing, waiting };
    private static final ArrayList<WorkerStatus> workerStatuses = new ArrayList<>();
    private static final Config config = new Config();

    private static final String WORD_SPOUT = "WORD_SPOUT";
    private static final String MAP_BOLT = "MAP_BOLT";
    private static final String REDUCE_BOLT = "REDUCE_BOLT";
    private static final String PRINT_BOLT = "PRINT_BOLT";

    /**
     * Registers a status page at "/status", which also accepts jobs on /submitJob URL
     */
    public static void registerStatusPage() {
        get("/status", (request, response) -> {
            response.type("text/html");

            StringBuilder html = new StringBuilder();

            html.append("<html><head><title>Master</title></head><body style='padding:25px'");
            html.append(getStatusTable(MasterServer.workerStatuses));
            html.append(getBreakLine());
            html.append(getJobsForm());
            html.append("</body></html>");

            return html.toString();
        });
    }

    public static void registerWorkerStatusPage() {
        get("/workerstatus", (request, response) -> {
            response.type("text/html");

            WorkerStatus workerStatus = new WorkerStatus(request.ip(),
                    request.queryParams("port"),
                    request.queryParams("status"),
                    request.queryParams("job"),
                    request.queryParams("keysRead"),
                    request.queryParams("keysWritten"),
                    request.queryParams("results"),
                    getCurrentTime()
            );

            MasterServer.workerStatuses.removeIf(match ->
                    match.getIpAddress().equals(workerStatus.getIpAddress()) &&
                            match.getPort() == workerStatus.getPort());

            MasterServer.workerStatuses.add(workerStatus);

            Set<String> queryParams = request.queryParams();

            return ("<html><head><title>Master</title></head>\n" +
                    "<body>Received a GET request on /workerstatus with params: " + queryParams + " </body></html>");
        });
    }

    private static void registerSubmitJob() {
        get("/submitJob", (request, response) -> {
            response.type("text/html");

            // TODO: We will receive the params for the job here, create topology and instantiate WorkerJob object

            //  1: Create config and insert stuff
            createMapReduceConfig(config,
                    request.queryParams("class"),
                    request.queryParams("input"),
                    request.queryParams("output"),
                    request.queryParams("mapThreads"),
                    request.queryParams("reduceThreads"));

            //  2: Create topology
            Topology topology = createTopology(config);

            //  3: Create Job
            WorkerJob job = new WorkerJob(topology, config);

            //  4: Send to all the workers
            try {
                ObjectMapper mapper = new ObjectMapper();
                mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);

                String[] workers = WorkerHelper.getWorkers(config);

                int i = 0;
                for (String dest : workers) {
                    config.put("workerIndex", String.valueOf(i++));
                    if (sendJob(dest, "POST", config, "definejob",
                            mapper.writerWithDefaultPrettyPrinter().writeValueAsString(job)).getResponseCode() !=
                            HttpURLConnection.HTTP_OK) {
                        throw new RuntimeException("Job definition request failed");
                    } else {
                        System.out.println("Sent 'job' to: " + dest);
                    }
                }
                for (String dest : workers) {
                    if (sendJob(dest, "POST", config, "runjob", "").getResponseCode() !=
                            HttpURLConnection.HTTP_OK) {
                        throw new RuntimeException("Job execution request failed");
                    } else {
                        System.out.println("Sent 'run job' to: " + dest);
                    }
                }
            }  catch (JsonProcessingException e) {
                e.printStackTrace();
                System.exit(0);
            }

            Set<String> queryParams = request.queryParams();
            return ("<html><head><title>Master</title></head>\n" +
                    "<body>Received a GET request on /submitJob with params: " + queryParams + " </body></html>");
        });
    }

    private static Topology createTopology(Config config) {
        /*
        When the administrator submits the web form on the /status page,
         the master node should first create an appropriate Topology using
         the FileSpout, MapBolt, ReduceBolt, PrintBolt or equivalent, and so
          on (see TestMapReduce for an example of how this works).   Given the
          Topology and a Config specifying the MapReduce parameters, the Master
           should instantiate a WorkerJob object.
         */

        FileSpout spout = new WordFileSpout();
        MapBolt bolt = new MapBolt();
        ReduceBolt bolt2 = new ReduceBolt();
        PrintBolt printer = new PrintBolt();

        TopologyBuilder builder = new TopologyBuilder();

        // Only one source ("spout") for the words
        builder.setSpout(WORD_SPOUT, spout, Integer.valueOf(config.get("spoutExecutors")));

        // Parallel mappers, each of which gets specific words
        builder.setBolt(MAP_BOLT, bolt, Integer.valueOf(config.get("mapExecutors"))).fieldsGrouping(WORD_SPOUT, new Fields("value"));

        // Parallel reducers, each of which gets specific words
        builder.setBolt(REDUCE_BOLT, bolt2, Integer.valueOf(config.get("reduceExecutors"))).fieldsGrouping(MAP_BOLT, new Fields("key"));

        // Only use the first printer bolt for reducing to a single point
        builder.setBolt(PRINT_BOLT, printer, 1).firstGrouping(REDUCE_BOLT);

        Topology topology = builder.createTopology();

        return topology;
    }

    private static void createMapReduceConfig(Config config, String jobClass, String input, String output, String mapThreads, String reduceThreads) {

        //  Job name
        config.put("job", jobClass);

        config.put("mapClass", jobClass);
        config.put("reduceClass", jobClass);

        config.put("input", input);
        config.put("output", output);

        //  Numbers of executors (per node)
        config.put("spoutExecutors", "1");
        config.put("mapExecutors", String.valueOf(Integer.parseInt(mapThreads)));
        config.put("reduceExecutors", String.valueOf(Integer.parseInt(reduceThreads)));

        for (String key : config.keySet()) {
            log.info(key + " | " + config.get(key));
        }

    }

    private static void addWorkersToConfig(Config config, String[] args) {

        //  TODO: Make this dynamic based on args. Question: We have the ports, but not the IP addresses
        config.put("master", "127.0.0.1:" + myPort);
        config.put("workerList", "[127.0.0.1:8001,127.0.0.1:8002]");
    }

    private static HttpURLConnection sendJob(String dest, String reqType, Config config, String job, String parameters) throws IOException {

        URL url = new URL(dest + "/" + job);

        log.info("Sending request to " + url.toString());

        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod(reqType);

        if (reqType.equals("POST")) {
            conn.setRequestProperty("Content-Type", "application/json");

            OutputStream os = conn.getOutputStream();
            byte[] toSend = parameters.getBytes();
            os.write(toSend);
            os.flush();
        } else
            conn.getOutputStream();

        return conn;
    }

    /**
     * The mainline for launching a MapReduce Master.  This should
     * handle at least the status and workerstatus routes, and optionally
     * initialize a worker as well.
     *
     * @param args
     */
    public static void main(String[] args) {
        port(myPort);

        addWorkersToConfig(config, args);
        registerStatusPage();
        registerWorkerStatusPage();
        registerSubmitJob();
    }

}
  
