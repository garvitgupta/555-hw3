package edu.upenn.cis.stormlite;

public class WorkerStatus {

    private final String ipAddress;
    private final int port;
    private final String status;
    private final String job;
    private final int keysRead;
    private final int keysWritten;
    private final String results;
    private final long statusTime;

    public WorkerStatus(String ipAddress,
                        String port,
                        String status,
                        String job,
                        String keysRead,
                        String keysWritten,
                        String results,
                        long currentTime) {

        this.ipAddress = ipAddress;
        this.port = Integer.parseInt(port);
        this.status = status;
        this.job = job;
        this.keysRead = Integer.parseInt(keysRead);
        this.keysWritten = Integer.parseInt(keysWritten);
        this.results = results;
        this.statusTime = currentTime;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public int getPort() {
        return port;
    }

    public String getStatus() {
        return status;
    }

    public String getJob() {
        return job;
    }

    public int getKeysRead() {
        return keysRead;
    }

    public int getKeysWritten() {
        return keysWritten;
    }

    public String getResults() {
        return results;
    }

    public long getStatusTime() {
        return statusTime;
    }
}