package edu.upenn.cis.stormlite;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Utilities {

    public static String getStatusTable(ArrayList<WorkerStatus> workerStatuses) {

        StringBuilder css = new StringBuilder();
        StringBuilder table = new StringBuilder();

        css.append("  <meta charset=\"utf-8\">\n" +
                "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css\">\n" +
                "  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\n" +
                "  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js\"></script>");

        table.append("<table class=\"table table-bordered table-striped\">");
        table.append("<tr>" +
                "<th>IP:Port</th>" +
                "<th>Status</th>" +
                "<th>Job</th>" +
                "<th>Keys Read</th>" +
                "<th>Keys Written</th>" +
                "</tr>");

        for (WorkerStatus workerStatus : workerStatuses) {

            if (workerStatus.getStatusTime() + 30000 < getCurrentTime()) {
                continue;
            }

            table.append("<tr>" +
                    "<td>" + workerStatus.getIpAddress() + ":" + workerStatus.getPort() + "</td>" +
                    "<td>" + workerStatus.getStatus() + "</td>" +
                    "<td>" + workerStatus.getJob() + "</td>" +
                    "<td>" + workerStatus.getKeysRead() + "</td>" +
                    "<td>" + workerStatus.getKeysWritten() + "</td>" +
                    "</tr>");
        }

        table.append("</table>");

        return css.toString() + table.toString();
    }

    public static String getBreakLine() {
        return "<br>";
    }


    public static String getJobsForm() {

        StringBuilder html = new StringBuilder();
        html.append("<form action='/submitJob' >\n" +
                "  <div class=\"form-group\">\n" +
                "    <label for=\"classname\">Class</label>\n" +
                "    <input name=\"class\" type=\"text\" class=\"form-control\" id=\"classname\" aria-describedby=\"classname\" value=\"edu.upenn.cis455.mapreduce.job.WordCount\" placeholder=\"edu.upenn.cis455.mapreduce.job.WordCount\">\n" +
                "    <small id=\"classname\" class=\"form-text text-muted\">Class name of the job</small>\n" +
                "  </div>\n" +
                "  <div class=\"form-group\">\n" +
                "    <label for=\"inputdirectory\">Input directory</label>\n" +
                "    <input name=\"input\" type=\"text\" class=\"form-control\" id=\"inputdirectory\" value=\"input\">\n" +
                "    <small id=\"classname\" class=\"form-text text-muted\">Relative to the storage directory (if this is set to bar and the storage directory is set to ~/foo, the input should be read from ~/foo/bar)</small>\n" +
                "  </div>\n" +
                "  \n" +
                " <div class=\"form-group\">\n" +
                "    <label for=\"inputdirectory\">Output directory</label>\n" +
                "    <input name=\"output\" type=\"text\" class=\"form-control\" id=\"inputdirectory\" value=\"output\">\n" +
                "    <small id=\"classname\" class=\"form-text text-muted\">Relative to the storage directory</small>\n" +
                "  </div>\n" +
                "\n" +
                "  <div class=\"form-group\">\n" +
                "    <label for=\"exampleFormControlSelect1\">Map threads</label>\n" +
                "    <select name=\"mapThreads\" class=\"form-control\" id=\"exampleFormControlSelect1\">\n" +
                "      <option>1</option>\n" +
                "      <option>2</option>\n" +
                "      <option>3</option>\n" +
                "      <option>4</option>\n" +
                "      <option>5</option>\n" +
                "    </select>\n" +
                "    <small id=\"classname\" class=\"form-text text-muted\">The number of map threads (MapBolt executors) to run on each worker</small>\n" +
                "  </div>\n" +
                "\n" +
                "  <div class=\"form-group\">\n" +
                "    <label for=\"exampleFormControlSelect1\">Reduce threads</label>\n" +
                "    <select name=\"reduceThreads\" class=\"form-control\" id=\"exampleFormControlSelect1\">\n" +
                "      <option>1</option>\n" +
                "      <option>2</option>\n" +
                "      <option>3</option>\n" +
                "      <option>4</option>\n" +
                "      <option>5</option>\n" +
                "    </select>\n" +
                "    <small id=\"classname\" class=\"form-text text-muted\">The number of reduce threads (ReduceBolt executors) to run on each worker</small>\n" +
                "  </div>\n" +
                "\n" +
                "  <button type=\"submit\" class=\"btn btn-primary\">Submit</button>\n" +
                "</form>");

        return html.toString();
    }

    public static long getCurrentTime() {
        Date date = new Date();
        return date.getTime();
    }

    public static String getCurrentTimestamp() {
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        return dateFormat.format(getCurrentTime());
    }
}
